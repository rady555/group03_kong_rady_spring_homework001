package com.example.group03_kong_rady_spring_homework001;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.LocalDateTime;

public class CustomerResponse<T> {

    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T customer;
    private String status;
    private LocalDateTime dateTime;

    public CustomerResponse(String message, T customer, String status, LocalDateTime dateTime) {
        this.dateTime = dateTime;
        this.status = status;
        this.message = message;
        this.customer = customer;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getCustomer() {
        return customer;
    }

    public void setCustomer(T customer) {
        this.customer = customer;
    }
}
