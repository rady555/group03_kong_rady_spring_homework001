package com.example.group03_kong_rady_spring_homework001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Group03KongRadySpringHomework001Application {

    public static void main(String[] args) {
        SpringApplication.run(Group03KongRadySpringHomework001Application.class, args);
        System.out.println("hi");
    }

}
