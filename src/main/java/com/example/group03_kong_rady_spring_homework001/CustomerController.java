package com.example.group03_kong_rady_spring_homework001;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1")
public class CustomerController {
    static int id = 4;
    ArrayList<Customer> cus = new ArrayList<>();
    public CustomerController(){
        cus.add(new Customer(1,"Koko","Female",22,"Phnom Penh"));
        cus.add(new Customer(2,"kaka","male",18,"Takeo"));
        cus.add(new Customer(3,"NaNa","Female",20,"Kampot"));
    }

//    get all the customer
    @GetMapping("/customers")
    public ResponseEntity<?> getAllCustomer(){
//        return cus;
        return ResponseEntity.ok(new CustomerResponse<ArrayList<Customer>>(
                "Your get all customer successfully!",
                cus,
                "OK",
                LocalDateTime.now()
        ));
    }

//    insert customer into array list
    @PostMapping("/customers")
    public ResponseEntity<?> insertCustomer(@RequestBody CustomerRequest customer){
        Customer customer1 = new Customer();

        customer1.setId(id);
        customer1.setName(customer.getName());
        customer1.setGender(customer.getGender());
        customer1.setAge(customer.getAge());
        customer1.setAddress(customer.getAddress());
        id++;
        cus.add(customer1);
        return ResponseEntity.ok(new CustomerResponse<Customer>(
                "This record was successfully created",
                customer1,
                "OK",
                LocalDateTime.now()
        ));
    }
//    get customer by id used @pathVariable
    @GetMapping("/customers/{customerId}")
    public ResponseEntity<?> getCustomerById(@PathVariable("customerId") Integer cusId){
        for (Customer  customer: cus){
            if (customer.getId() == cusId){
//                return customer;
                return ResponseEntity.ok(new CustomerResponse<>(
                        "This record has found successfully",
                        customer,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }

//    get customer by name used @requestparam
    @GetMapping("/customers/search")
    public ResponseEntity<?> getCustomerByName(@RequestParam String cusName){
        for (Customer  customer: cus){
            if (customer.getName().equals(cusName)){
                return ResponseEntity.ok(new CustomerResponse<>(
                        "This record has found successfully",
                        customer,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }

//    update customer by id
    @PutMapping("/customers/{customerId}")
    public ResponseEntity <?> update(@RequestBody CustomerRequest customer, @PathVariable Integer customerId) {
        Customer cusUp = null;
        for (Customer c : cus) {
            if (c.getId() == customerId) {
                c.setName(customer.getName());
                c.setGender(customer.getGender());
                c.setAge(customer.getAge());
                c.setAddress(customer.getAddress());
                cusUp = new Customer(c.getId(),customer.getName(),customer.getGender(), customer.getAge(), customer.getAddress());
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                    "You're update successfully",
                    cusUp,
                    "OK",
                    LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }

//    delete customer by id
    @DeleteMapping("/customers/{customerId}")
    public ResponseEntity <?> delete(@RequestBody CustomerRequest customer, @PathVariable Integer customerId){
        for(Customer c: cus){
            if((c.getId()+1) == customerId){
                cus.remove(c.getId());
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "Congratulation your delete is successfully",
                        null,
                        "OK",
                        LocalDateTime.now()
                ));
            }else{
                return ResponseEntity.ok("The save with update not found id.");
            }
        }
        return null;
    }
}

